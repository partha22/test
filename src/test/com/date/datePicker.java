package test.com.date;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.interactions.Actions;

public class datePicker {
	
	public static WebDriver driver;

	public static void main(String[] args) throws Exception {
		
		
		// TODO Auto-generated method stub
		String strChromeDriverPath = "D:\\Selenium_Test\\Test\\src\\resource\\chromeDriver\\new\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", strChromeDriverPath);
		driver = new ChromeDriver();
		//dragDrop(driver);
		//doubleClick(driver);
		rightClick(driver);
		//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		/* driver.get("https://www.cleartrip.com/");
		
		WebElement roundTrip = driver.findElement(By.xpath("//*[@id='RoundTrip']"));
		roundTrip.click();
		
		//WebElement date = driver.findElement(By.xpath("//*[@class='icon ir datePicker']"));
		
		WebElement date = driver.findElement(By.xpath("//*[@id='DepartDate']"));
		Actions a = new Actions(driver);
		a.moveToElement(date).click().build().perform();
		//WebElement dateWizard = driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div[1]/table/tbody"));
		WebElement dateWizard = driver.findElement(By.xpath("//*[@class='monthBlock first']/table/tbody"));
		List<WebElement> columns = dateWizard.findElements(By.tagName("td"));
		for (WebElement cell : columns)
		{
			if(cell.getText().equals("21")){
				cell.click();
				//WebElement departDate = driver.findElement(By.xpath("//*[@id='DepartDate']"));
				//departDate.sendKeys(cell.getText());
				break;
			}
		}
		
				
		WebElement date1 = driver.findElement(By.xpath("//*[@id='ReturnDate']"));
		Actions a1 = new Actions(driver);
		a1.moveToElement(date1).click().build().perform();
		
		WebElement dateWizard1 = driver.findElement(By.xpath("//*[@class='monthBlock last']/table/tbody"));
		List<WebElement> columns1 = dateWizard1.findElements(By.tagName("td"));
		for (WebElement cell1 : columns1)
		{
			if(cell1.getText().equals("22")){
				cell1.click();
				break;
			}
		}
		
		// pass string by Robot class
		WebElement fromLocation = driver.findElement(By.xpath("//input[@id='FromTag']"));
		Actions a2 = new Actions(driver);
		a1.moveToElement(fromLocation).click().build().perform();
		Robot rbt = new Robot();
		//rbt.keyPress(KeyEvent.VK_a);
		String dt = "KOLKATA";
		for (int i= 0 ; i < dt.length();++i){
			char c = dt.charAt(i);
			System.out.println(c);
			int keyCode = KeyEvent.getExtendedKeyCodeForChar(c);
			rbt.keyPress(keyCode);
			
		}
		
		
		Thread.sleep(3000);
		WebElement fromLocation1 = driver.findElement(By.xpath("//input[@id='FromTag']"));
		String text = fromLocation1.getText();
		System.out.println(text);

				
		//Right click in the TextBox
		//Actions action = new Actions(driver);
		//WebElement fromLocation = driver.findElement(By.xpath("//input[@id='FromTag']"));
		//a2.contextClick(fromLocation).build().perform();
		//Thread.sleep(3000);
		a2.contextClick(fromLocation).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(3000);

				
		WebElement toLocation = driver.findElement(By.xpath("//input[@id='ToTag']"));
		/*Actions a3 = new Actions(driver);
		a3.moveToElement(toLocation).click().build().perform();
		Robot rbt1 = new Robot();
		String dtTo = "KOLKATA";
		for (char c : dtTo.toCharArray()){
		System.out.println(c);
		//char c = dt.charAt(i);
		int keyCode = KeyEvent.getExtendedKeyCodeForChar(c);
		rbt1.keyPress(keyCode);
					
	}*/
	/*	Actions a3 = new Actions(driver);
		a3.moveToElement(toLocation).click().build().perform();
		a3.contextClick(toLocation).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		
		*/		
		 

	}
	
	/* TEST DATE PICKER */
	
	public static void dragDrop(WebDriver driver)
	{
		driver.get("http://demo.guru99.com/test/drag_drop.html");
		
		WebElement From=driver.findElement(By.xpath("//a[contains(text(),'BANK')]"));	
        
        	
        WebElement To=driver.findElement(By.xpath("//ol[@id='bank']//li[@class='placeholder']"));					
        		
        Actions act=new Actions(driver);					

		
        act.dragAndDrop(From, To).build().perform();		
	}
	
	
	public static void doubleClick(WebDriver driver)
	{
		driver.get("http://demo.guru99.com/test/drag_drop.html");
		
		WebElement element =driver.findElement(By.xpath("//div[@id='shoppingCart1']//h3[@class='ui-widget-header'][contains(text(),'Account')]"));	
          		
        	
        Actions act=new Actions(driver);					
		
        act.doubleClick(element).build().perform();
	}

	
	public static void rightClick(WebDriver driver) throws Exception
	{
		driver.get("http://demo.guru99.com/test/drag_drop.html");
		
		WebElement element =driver.findElement(By.xpath("//b[@class='caret']"));
		WebElement element1 =driver.findElement(By.xpath("//a[contains(text(),'Flash Movie Demo')]"));
		Robot rb = new Robot();
                 	
		Actions a3 = new Actions(driver);
		a3.moveToElement(element).click().build().perform();
		rb.keyPress(KeyEvent.VK_DOWN);
		a3.contextClick(element1).build().perform();
		rb.keyPress(KeyEvent.VK_RIGHT);
		//a3.sendKeys("t").perform();
		a3.click().build().perform();
				
	}

}
